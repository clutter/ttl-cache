"use strict";

const EventEmitter = require("events");

const MILLISECS = 1000; // 30-sec
const HALF_MINUTE = 30000; // 30-sec
const MINUTE = 60000; // 1-min
const DEFAULT_TTL = 900000; // 15-min

var cacheObjects = [],
    ttlTimer;

function getNow() {
    return Math.floor(Date.now() / MILLISECS) * MILLISECS; // round-off to nearest sec
}

if (!ttlTimer) {
    ttlTimer = setInterval(function ttlCheck() {
        let now = getNow();

        cacheObjects.forEach(function prune(obj) {
            obj.keys().forEach(function eachKey(key) {
                if (obj._cache[key].ttl <= now) {
                    obj.del(key);
                }
            });
        });
    }, HALF_MINUTE); // for 1-min accuracy
    ttlTimer.unref();
}

class TTLCache extends EventEmitter {
    constructor(inpOpts) {
        let opts;

        super();
        opts = Object.assign({}, inpOpts);
        this._cache = {};
        this._ttl = Number(opts.ttl) ? Number(opts.ttl) * MINUTE : DEFAULT_TTL;
        this._extend = Boolean(opts.extend);
        cacheObjects.push(this);
    }

    set(key, value) {
        if (key === undefined || value === undefined) {
            return;
        }
        if (this._cache[key]) {
            this.del(key);
        }
        this._cache[key] = {
            "val": value,
            "ttl": getNow() + this._ttl
        };
    }

    get(key) {
        let obj = this._cache[key];

        if (obj) {
            if (this._extend) {
                obj.ttl = getNow() + this._ttl;
            }
            obj = obj.val;
        }

        return obj;
    }

    ttl(key) {
        let obj = this._cache[key];

        if (obj) {
            obj = obj.ttl;
        }

        return obj;
    }

    del(key) {
        let obj = this._cache[key];

        if (obj) {
            this.emit("evict", key, obj.val);
        }
        delete this._cache[key];
    }

    size() {
        return Object.keys(this._cache).length;
    }

    keys() {
        return Object.keys(this._cache);
    }

    values() {
        let arr = [];

        Object.values(this._cache).forEach(function eachObj(obj) {
            arr.push(obj.val);
        });

        return arr;
    }

    clear() {
        let self = this;

        this.keys().forEach(function eachKey(key) {
            self.del(key);
        });
    }

    unref() {
        cacheObjects.splice(cacheObjects.indexOf(this), 1); // eslint-disable-line no-magic-numbers
    }
}

module.exports = TTLCache;
