# TTL Cache
TTL cache with an accuracy of 1-minute for evicting items

## Installation

```bash
$ npm install @parthar/ttl-cache --save
```

## Usage

```js
// create
var TTLCache = require("@parthar/ttl-cache");
var cache = new TTLCache({
    ttl: 15, // Integer - key-ttl in minutes; default: 15-min
    extend: false // boolean - extend TTL on get; default: false
});

// events
cache.on("evict", function(key, val) { }); // key evicted

// methods
cache.set("ping", "pong");
cache.set("ying", "yang");
cache.get("ping"); // pong
cache.get("ying"); // yang
cache.get("foo"); // undefined
cache.size(); // 2
cache.keys(); // ["ping", "ying"]
cache.values(); // ["pong", "yang"]
cache.ttl("ying"); // milli-secs since epoch at which key will expire
cache.del("ping"); // evict event ping/pong
cache.get("ping"); // undefined
cache.size(); // 1

// after 15-min; evict event for ying/yang
cache.get("ping"); // undefined
cache.get("ying"); // undefined
cache.get("foo"); // undefined
cache.size(); // 0

// clean-up
cache.clear(); // del all keys
cache.unref(); // stop ttl checks
```
