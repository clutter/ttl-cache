/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
const TTLCache = require("../");

var cache;

before(function (done) {
    cache = new TTLCache({
        "ttl": 1,
        "extend": true
    });
    done();
});

describe("TTLCache", function () {
    it("should be a class", function (done) {
        assert.strictEqual(typeof TTLCache, "function");
        done();
    });
    it("should set key/value readable via get", function (done) {
        cache.set("ping", "pong");
        cache.set("ying", "yang");
        assert.strictEqual(cache.get("ping"), "pong");
        assert.strictEqual(cache.get("ying"), "yang");
        done();
    });
    it("should return number of keys via size", function (done) {
        assert.strictEqual(cache.size(), 2);
        done();
    });
    it("should return all keys", function (done) {
        assert.deepEqual(cache.keys(), ["ping", "ying"]);
        done();
    });
    it("should return all values", function (done) {
        assert.deepEqual(cache.values(), ["pong", "yang"]);
        done();
    });
    it("should get undefined for non-existent keys", function (done) {
        assert.strictEqual(cache.get("foo"), undefined);
        assert.strictEqual(cache.get("bar"), undefined);
        done();
    });
    it("should del key and emit evict-event", function (done) {
        let evt = false;

        cache.once("evict", function (key, val) {
            assert.strictEqual(key, "ping");
            assert.strictEqual(val, "pong");
            evt = true;
        });
        cache.del("ping");
        assert.strictEqual(cache.get("ping"), undefined);
        setImmediate(function () {
            assert.strictEqual(evt, true, "evict event");
            done();
        });
    });
    it("should retain other data after del", function (done) {
        assert.strictEqual(cache.get("ying"), "yang");
        assert.strictEqual(cache.size(), 1);
        assert.strictEqual(cache.get("foo"), undefined);
        done();
    });
    it("should clear cache", function (done) {
        cache.clear();
        assert.strictEqual(cache.get("ping"), undefined);
        assert.strictEqual(cache.get("ying"), undefined);
        assert.strictEqual(cache.get("foo"), undefined);
        assert.strictEqual(cache.size(), 0);
        done();
    });
    it("starting ttl test - approx 1-min to test...", function (done) {
        done();
    });
    it("should enforce ttl", function (done) {
        let evt = false;

        this.timeout(90000); // eslint-disable-line no-invalid-this
        cache.once("evict", function (key, val) {
            assert.strictEqual(key, "ping");
            assert.strictEqual(val, "pong");
            evt = true;
        });
        cache.set("ping", "pong");
        cache.set("ying", "yang");
        assert.strictEqual(cache.size(), 2);
        setTimeout(function () {
            assert.strictEqual(cache.get("ying"), "yang");
            assert.strictEqual((Date.now() - cache.ttl("ying")) < 10, true);
        }, 30000);
        setTimeout(function () {
            assert.strictEqual(cache.get("ping"), undefined);
            assert.strictEqual(evt, true, "evict event");
            done();
        }, 70000);
    });
});

after(function (done) {
    cache.clear();
    cache.unref();
    cache = null;
    done();
});
